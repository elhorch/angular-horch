import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class HelloWordService {

  constructor(private http: HttpClient) { }

  private apiServer = "http://localhost:8080";

  helloWorldService() {
    //const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa('javaguides' + ':' + 'password') });
    return this.http.get<Message>(this.apiServer+'/api/v1/greeting');
  }

  detailsService() {
    return this.http.get<string[]>(this.apiServer+'/api/v1/details');
  }

}
