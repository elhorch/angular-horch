import { Component, OnInit } from '@angular/core';
import {HelloWordService} from '../hello-word.service';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../login/auth.service";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {


  constructor(
    private helloWorldService: HelloWordService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) {   }

  details : string[];

  ngOnInit(): void {
    console.log('details Component');
    this.helloWorldService.detailsService().subscribe((result) => {
      this.details = result;
    });
  }

}
