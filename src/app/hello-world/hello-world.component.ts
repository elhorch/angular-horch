import { Component, OnInit } from '@angular/core';
import { Message } from '../message';
import { HelloWordService } from '../hello-word.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {

  message: string;

  constructor(private helloWorldService: HelloWordService,
              private route: ActivatedRoute,
              private router: Router
  ) { }

  ngOnInit() {

    console.log('HelloWorldComponent');
    this.helloWorldService.helloWorldService().subscribe( (result) => {
      this.message = result.content;
    });
  }

  handleDetails() {
    this.router.navigate(['/details']);
  }

}
